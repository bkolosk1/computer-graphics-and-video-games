//63170367
let fileData = null;
let vertexCount = 0;
let vereticies = [];
let faces = [];
let updatedVerticies = [];
let modelViewMatrix = Matrix4();
let canvas = null;
let context = null;
let width = 1200;
let height = 600;
let delta = 5;

window.onload = function(){
  canvas = document.getElementById('canvas');
  context = canvas.getContext('2d');  
}

//Resets fileData, vertexCount
function memset(){
    fileData = null;
    vertexCount = 0;
    vereticies = [];
    faces = [];
    updatedVerticies = [];
    modelViewMatrix = matrixMultiply(scale(200, -200, 200), modelViewMatrix); 
}

//Returns 3x3 identity matrix
function Matrix3(){
    let matrix = new Float32Array(9);
    matrix[indexTransform3(0,0)] = 1;
    matrix[indexTransform3(1,1)] = 1;
    matrix[indexTransform3(2,2)] = 1;
    return matrix;
}
//Returns 4x4 identity matrix
function Matrix4(){
  let matrix = new Float32Array(16);
  matrix[indexTransform4(0,0)] = 1;
  matrix[indexTransform4(1,1)] = 1;
  matrix[indexTransform4(2,2)] = 1;
  matrix[indexTransform4(3,3)] = 1;
  return matrix;
}
//Returns Vector3
function Vector3(){
  return new Float32Array(3);
}

//Returns Vector4
function Vector4(){
  return new Float32Array(4);
}

//Dot product
function dot(vectorA,vectorB){
  let size = vectorA.length;
  let vector = new Float32Array(size); 
  for(let i=0;i<size;i++){
    vector[i] = vectorA[i]*vectorB[i];
  }
  return vector;
}

//Normalize
function normalize(vectorA){
  let len = 0;
  for(let i = 0;i<4;i++) {
    len += vectorA[i]*vectorA[i];
  }
  len = Math.sqrt(len);
  let vector = new Vector4();
  for(let i =0;i<4;i++){
    vector[i] = vectorA[i]/len;
  }
  return vector;
}
// Normalizwe coords to the metacoord
function coordinateNormalize(vector){
  let resultVector = Vector4();
  console.log()
  for(let i=0;i<4;resultVector[i]=vector[i],i++);
  if(resultVector[3]!=1 && resultVector[3]!=0){
    for(let i=0;i<4;resultVector[i]/=resultVector[3],i++);
  }
  return resultVector;
}

//Multiply matrixA by matrixB
function matrixMultiply(matrixA,matrixB){
  let size = Math.sqrt(matrixA.length);
  let matrix = new Float32Array(size*size);
  for (let i = 0; i < size; i++){
      for (var j = 0; j < size; j++) { 
          let tmp = 0;
          for (var k = 0; k < size; k++){
             tmp = tmp + matrixA[indexTransform4(i,k)]*matrixB[indexTransform4(k,j)];
          }
          matrix[indexTransform4(i,j)] = tmp;
      }
  }
  return matrix;
}

//Multiply vectorB with MatrixA
function vectorMultiply(matrixA,vectorB){
  let size = vectorB.length;
  let vector = new Float32Array(size);
  for(let i=0;i<size;i++){
    for(let j=0;j<size;j++){
      vector[i] = vector[i] + vectorB[j]*matrixA[indexTransform4(i,j)];
    }
  }
  return vector;
}

//Daws line between x and y
function drawLine(x,y){
  context.beginPath();
  context.moveTo(updatedVerticies[x][0], updatedVerticies[x][1]);
  context.lineTo(updatedVerticies[y][0], updatedVerticies[y][1]);
  context.stroke();
  context.strokeStyle = "#FFFF00";

}

//Converts degrees to radians
function degToRad(alpha) {
  return alpha*(Math.PI/180);
}


function calculateTransform() {
  if(vereticies.length * faces.length > 0) {
      context.clearRect(0, 0,width,height);
      let updateMatrix = Matrix4();
      updateMatrix = matrixMultiply(translate(0,0,-8),modelViewMatrix);
	  // updateMatrix = matrixMultiply(perspective(4), updateMatrix);
      updateMatrix = matrixMultiply(translate(width/2,height/2,0),updateMatrix);
      for(let i=0; i < vereticies.length; i++){
        updatedVerticies.push(transform(updateMatrix,vereticies[i]));
      }
      //Draw triangles
      for(let i=0;i < faces.length; i++){
        drawLine(faces[i][0]-1,faces[i][1]-1);
        drawLine(faces[i][0]-1,faces[i][2]-1);
        drawLine(faces[i][1]-1,faces[i][1]-1);
      }
      updatedVerticies = [];
  }
}

function transform(matrix, vector) {
  let transformation = Vector4();
  transformation = vectorMultiply(matrix, vector);
  transformation = coordinateNormalize(transformation);  
  return transformation;
}

function indexTransform3(x,y){
  return 3*x + y;
}

function indexTransform4(x,y){
  return 4*x + y;
}

//<mat4> rotateX(<float> alpha);
function rotateX(alpha){
  let resultMatrix = Matrix4();
  let sin_alpha = Math.sin(degToRad(alpha));
  let cos_alpha = Math.cos(degToRad(alpha));
  resultMatrix[indexTransform4(1,1)] = cos_alpha
  resultMatrix[indexTransform4(1,2)] = -sin_alpha
  resultMatrix[indexTransform4(2,1)] = sin_alpha
  resultMatrix[indexTransform4(2,2)] = cos_alpha
  return resultMatrix;
}

//<mat4> rotateY(<float> alpha);
function rotateY(alpha){
  let resultMatrix = Matrix4();
  let sin_alpha = Math.sin(degToRad(alpha));
  let cos_alpha = Math.cos(degToRad(alpha));
  resultMatrix[indexTransform4(0,0)] = cos_alpha
  resultMatrix[indexTransform4(0,2)] = sin_alpha
  resultMatrix[indexTransform4(2,0)] = -sin_alpha
  resultMatrix[indexTransform4(2,2)] = cos_alpha
  return resultMatrix;
}

//<mat4> rotateZ(<float> alpha);
function rotateZ(alpha){
  let resultMatrix = Matrix4();
  let sin_alpha = Math.sin(degToRad(alpha));
  let cos_alpha = Math.cos(degToRad(alpha));
  resultMatrix[indexTransform4(0,0)] = cos_alpha
  resultMatrix[indexTransform4(0,1)] = -sin_alpha
  resultMatrix[indexTransform4(1,0)] = sin_alpha
  resultMatrix[indexTransform4(1,1)] = cos_alpha
  return resultMatrix;
}

//<mat4> translate(<float> dx, <float> dy, <float> dz);
function translate(dx,dy,dz){
    let resultMatrix = Matrix4();
    resultMatrix[indexTransform4(0,3)] = dx;
    resultMatrix[indexTransform4(1,3)] = dy;
    resultMatrix[indexTransform4(2,3)] = dz;
    return resultMatrix;
}

//<mat4> scale(<float> sx, <float> sy, <float> sz);
function scale(sx,sy,sz){
  let resultMatrix = Matrix4();
  resultMatrix[indexTransform4(0,0)] = sx;
  resultMatrix[indexTransform4(1,1)] = sy;
  resultMatrix[indexTransform4(2,2)] = sz;
  return resultMatrix;
}

//<mat4> perspective(<float> d); // primerna vrednost je d=4
function perspective(d){
  let resultMatrix = Matrix4();
  resultMatrix[indexTransform4(3,2)] =  1/d;
  resultMatrix[indexTransform4(3,3)] = 0;
  return resultMatrix;
}

//Open file 
function openFile(event) {
  memset();
  let file = event.target.files[0];
  let reader = new FileReader();
  reader.onloadend = function() {
      fileData = reader.result;
      parseOBJ(fileData);      
  };  
  reader.readAsText(file);
}

function parseOBJ(fileData){
  var lines = fileData.split("\n");
  for(let iter = 0;iter<lines.length;iter++){
    let line = lines[iter].split(" ");
    if(line.length > 1){
      if(line[0] == "v"){
         parseVertex(line);
        }
      if(line[0] == "f"){
          parseFace(line);
      }
    }
  }  
  calculateTransform();
}

function parseVertex(line){
  vertexCount++;
  let tmp_vertex = Vector4();
  for(let iter = 0;iter < 3;tmp_vertex[iter] = parseFloat(line[iter+1]),iter++);
  tmp_vertex[3] = 1;
  tmp_vertex=normalize(tmp_vertex);
  vereticies.push(tmp_vertex);
}

function parseFace(line){
  let tmp_face = Vector3();
  for(let iter = 0;iter < 3;tmp_face[iter] = parseFloat(line[iter+1]),iter++);
  faces.push(tmp_face);  
}

document.onkeydown =  function (e){
  switch(e.which) {
      case 87: // W
          modelViewMatrix = matrixMultiply(rotateX(delta),modelViewMatrix);
          break;
      case 83: // S
          modelViewMatrix = matrixMultiply(rotateX(-delta),modelViewMatrix);
          break;
      case 68: // D
          modelViewMatrix = matrixMultiply(rotateY(-delta),modelViewMatrix);
          break;
      case 65: // A
          modelViewMatrix = matrixMultiply(rotateY(delta),modelViewMatrix);
          break;
      case 69: // E
          modelViewMatrix = matrixMultiply(rotateZ(-delta),modelViewMatrix);
          break;
      case 81: // Q
          modelViewMatrix = matrixMultiply(rotateZ(delta),modelViewMatrix);
          break;
      case 38: // UP
          modelViewMatrix = matrixMultiply(translate(0, -delta, 0),modelViewMatrix);
          break;
      case 40: // DOWN
          modelViewMatrix = matrixMultiply(translate(0, delta, 0),modelViewMatrix);
          break;
      case 39: // RIGHT
          modelViewMatrix = matrixMultiply(translate(delta, 0, 0),modelViewMatrix);
          break;
      case 37: // LEFT
          modelViewMatrix = matrixMultiply(translate(-delta, 0, 0),modelViewMatrix);
          break;  
      case 107: // +
          modelViewMatrix = matrixMultiply(scale(1.1, 1.1, 1.1), modelViewMatrix);
          break;
      case 109: // -
          modelViewMatrix = matrixMultiply(scale(1/1.1, 1/1.1, 1/1.1), modelViewMatrix);
          break;
      case 82: // R
          modelViewMatrix = Matrix4();
          modelViewMatrix = matrixMultiply(scale(100, -100, 100), modelViewMatrix); 
          break;    
      default:
          alert("Wrong button!");
          return;
  }
  calculateTransform();
}