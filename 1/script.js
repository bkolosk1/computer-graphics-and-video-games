let fileData = null;
let vertexCount = 0;
let vereticies = [];
let faces = [];
let normals = [];
let lightPositions = [];
let lightColors = [];
let materials = {};
let updatedVerticies = [];
let vertexColor = [];
let modelViewMatrix = Matrix4();
let canvas = null;
let context = null;
let lightCount = 0;
let width = 1200;
let height = 600;
let delta = 5;

window.onload = function(){
  canvas = document.getElementById('canvas');
  context = canvas.getContext('2d');  
}

//Resets fileData, vertexCount
function memset(){
    fileData = null;
    vertexCount = 0;
    vereticies = [];
    faces = [];
    updatedVerticies = [];
    modelViewMatrix = matrixMultiply(scale(200, -200, 200), modelViewMatrix); 
}

//Returns 3x3 identity matrix
function Matrix3(){
    let matrix = new Float32Array(9);
    matrix[indexTransform3(0,0)] = 1;
    matrix[indexTransform3(1,1)] = 1;
    matrix[indexTransform3(2,2)] = 1;
    return matrix;
}
//Returns 4x4 identity matrix
function Matrix4(){
  let matrix = new Float32Array(16);
  matrix[indexTransform4(0,0)] = 1;
  matrix[indexTransform4(1,1)] = 1;
  matrix[indexTransform4(2,2)] = 1;
  matrix[indexTransform4(3,3)] = 1;
  return matrix;
}
//Returns Vector3
function Vector3(){
  return new Float32Array(3);
}

//Returns Vector4
function Vector4(){
  return new Float32Array(4);
}

//Dot product
function dot(vectorA,vectorB){
  if(vectorA.length == vectorB.length){
    let size = vectorA.length;
    let rez = 0; 
    for(let i=0;i<size;i++){
      rez += vectorA[i]*vectorB[i];
    }
    return rez;
  }
}

//Normalize
function normalize(vectorA){
  let len = 0;
  for(let i = 0;i<4;i++) {
    len += vectorA[i]*vectorA[i];
  }
  len = Math.sqrt(len);
  let vector = new Vector4();
  for(let i =0;i<4;i++){
    vector[i] = vectorA[i]/len;
  }
  return vector;
}

function normalize3(vectorA){
  let len = 0;
  for(let i = 0;i<3;i++) {
    len += vectorA[i]*vectorA[i];
  }
  len = Math.sqrt(len);
  let vector = new Vector3();
  for(let i =0;i<3;i++){
    vector[i] = vectorA[i]/len;
  }
  return vector;
}

// Normalizwe coords to the metacoord
function coordinateNormalize(vector){
  let resultVector = Vector4();
  for(let i=0;i<4;resultVector[i]=vector[i],i++);
  if(resultVector[3]!=1 && resultVector[3]!=0){
    for(let i=0;i<4;resultVector[i]/=resultVector[3],i++);
  }
  return resultVector;
}

//Multiply matrixA by matrixB
function matrixMultiply(matrixA,matrixB){
  let size = Math.sqrt(matrixA.length);
  let matrix = new Float32Array(size*size);
  for (let i = 0; i < size; i++){
      for (var j = 0; j < size; j++) { 
          let tmp = 0;
          for (var k = 0; k < size; k++){
             tmp = tmp + matrixA[indexTransform4(i,k)]*matrixB[indexTransform4(k,j)];
          }
          matrix[indexTransform4(i,j)] = tmp;
      }
  }
  return matrix;
}

//Multiply vectorB with MatrixA
function vectorMultiply(matrixA,vectorB){
  let size = vectorB.length;
  let vector = new Float32Array(size);
  for(let i=0;i<size;i++){
    for(let j=0;j<size;j++){
      vector[i] = vector[i] + vectorB[j]*matrixA[indexTransform4(i,j)];
    }
  }
  return vector;
}

//Daws line between x and y
function drawLine(x,y){
  context.beginPath();
  context.moveTo(updatedVerticies[x][0], updatedVerticies[x][1]);
  context.lineTo(updatedVerticies[y][0], updatedVerticies[y][1]);  
  context.stroke();
  context.strokeStyle = "#FFFF00";

}

function rgb2string(idx){
  return "rgb("+updateColors[idx][0] * 255+", "+updateColors[idx][1] * 255+", "+updateColors[idx][2] * 255+")";
}

function rgb2string2(clr){
  return "rgb("+clr[0] * 255+", "+clr[1] * 255+", "+clr[2] * 255+")";
}

function drawLine2(x,y){
  let dx1 = updatedVerticies[x][0]; let dy1 = updatedVerticies[x][1];
  let dx2 = updatedVerticies[y][0]; let dy2 = updatedVerticies[y][1];
  var grad= context.createLinearGradient(dx1, dy1, dx2, dy2);
  grad.addColorStop(0, rgb2string(x));
  grad.addColorStop(1, rgb2string(y));
  context.strokeStyle = grad;
  context.beginPath();
  context.moveTo(updatedVerticies[x][0], updatedVerticies[x][1]);
  context.lineTo(updatedVerticies[y][0], updatedVerticies[y][1]);  
  context.stroke();
}

function avg(x,y,z){
  let c = new Vector3();
  c[0] = (updateColors[x][0]+updateColors[y][0]+updateColors[z][0])/3;
  c[1] = (updateColors[x][1]+updateColors[y][1]+updateColors[z][1])/3;
  c[2] = (updateColors[x][2]+updateColors[y][2]+updateColors[z][2])/3;
  return c;
}

function centralPoint(x,y,z){
  let c = new Vector3();
  c[0] = (updatedVerticies[x][0]+updatedVerticies[y][0]+updatedVerticies[z][0])/3;
  c[1] = (updatedVerticies[x][1]+updatedVerticies[y][1]+updatedVerticies[z][1])/3;
  c[2] = (updatedVerticies[x][2]+updatedVerticies[y][2]+updatedVerticies[z][2])/3;
  return c;
}

function drawTrigs(x,y,z){
  console.log(x,y,z);
  context.beginPath();
  context.moveTo(updatedVerticies[x][0], updatedVerticies[x][1]);
  context.lineTo(updatedVerticies[y][0], updatedVerticies[y][1]);
  context.lineTo(updatedVerticies[z][0], updatedVerticies[z][1]);
  context.fill();
  let color = avg(x,y,z);
  console.log(rgb2string2(color));
  context.fillStyle = rgb2string2(color);
  context.fill();
}

//Converts degrees to radians
function degToRad(alpha) {
  return alpha*(Math.PI/180);
}


function calculateTransform() {
  if(vereticies.length * faces.length > 0) {
      updateLights();
      context.clearRect(0, 0, canvas.width, canvas.height);      
      let updateMatrix = Matrix4();
      updateMatrix = matrixMultiply(translate(0,0,-8),modelViewMatrix);
	  // updateMatrix = matrixMultiply(perspective(4), updateMatrix);
      updateMatrix = matrixMultiply(translate(width/2,height/2,0),updateMatrix);
      for(let i=0; i < vereticies.length; i++){
        updatedVerticies.push(transform(updateMatrix,vereticies[i]));
      }
      if(controlDraw == 0){
        for(let i=0;i < faces.length; i++){
          drawLine(faces[i][0]-1,faces[i][1]-1);
          drawLine(faces[i][0]-1,faces[i][2]-1);
          drawLine(faces[i][2]-1,faces[i][1]-1);
        }
      }
      else if(controlDraw == 1){
        //SortTrigs
        faces.sort(function(a,b){
          let centerA = centralPoint(a[0]-1,a[1]-1,a[2]-1)
          let centerB = centralPoint(b[0]-1,b[1]-1,b[2]-1)
          return centerA[2] - centerB[2];
        });
        //Draw triangles
        for(let i=0;i < faces.length; i++){
          drawTrigs(faces[i][0]-1,faces[i][1]-1,faces[i][2]-1);
        }
      }
      else{
        for(let i=0;i < faces.length; i++){
          drawLine2(faces[i][0]-1,faces[i][1]-1);
          drawLine2(faces[i][0]-1,faces[i][2]-1);
          drawLine2(faces[i][2]-1,faces[i][1]-1);
        }
      }
      /*
      for(let i=0;i < faces.length; i++){
        drawLine(faces[i][0]-1,faces[i][1]-1);
        drawLine(faces[i][0]-1,faces[i][2]-1);
        drawLine(faces[i][2]-1,faces[i][1]-1);
      }
      */
      updatedVerticies = [];
      updateColors = [];
  }
}

function transform(matrix, vector) {
  let transformation = Vector4();
  transformation = vectorMultiply(matrix, vector);
  transformation = coordinateNormalize(transformation);  
  return transformation;
}

function indexTransform3(x,y){
  return 3*x + y;
}

function indexTransform4(x,y){
  return 4*x + y;
}

//<mat4> rotateX(<float> alpha);
function rotateX(alpha){
  let resultMatrix = Matrix4();
  let sin_alpha = Math.sin(degToRad(alpha));
  let cos_alpha = Math.cos(degToRad(alpha));
  resultMatrix[indexTransform4(1,1)] = cos_alpha
  resultMatrix[indexTransform4(1,2)] = -sin_alpha
  resultMatrix[indexTransform4(2,1)] = sin_alpha
  resultMatrix[indexTransform4(2,2)] = cos_alpha
  return resultMatrix;
}

//<mat4> rotateY(<float> alpha);
function rotateY(alpha){
  let resultMatrix = Matrix4();
  let sin_alpha = Math.sin(degToRad(alpha));
  let cos_alpha = Math.cos(degToRad(alpha));
  resultMatrix[indexTransform4(0,0)] = cos_alpha
  resultMatrix[indexTransform4(0,2)] = sin_alpha
  resultMatrix[indexTransform4(2,0)] = -sin_alpha
  resultMatrix[indexTransform4(2,2)] = cos_alpha
  return resultMatrix;
}

//<mat4> rotateZ(<float> alpha);
function rotateZ(alpha){
  let resultMatrix = Matrix4();
  let sin_alpha = Math.sin(degToRad(alpha));
  let cos_alpha = Math.cos(degToRad(alpha));
  resultMatrix[indexTransform4(0,0)] = cos_alpha
  resultMatrix[indexTransform4(0,1)] = -sin_alpha
  resultMatrix[indexTransform4(1,0)] = sin_alpha
  resultMatrix[indexTransform4(1,1)] = cos_alpha
  return resultMatrix;
}

//<mat4> translate(<float> dx, <float> dy, <float> dz);
function translate(dx,dy,dz){
    let resultMatrix = Matrix4();
    resultMatrix[indexTransform4(0,3)] = dx;
    resultMatrix[indexTransform4(1,3)] = dy;
    resultMatrix[indexTransform4(2,3)] = dz;
    return resultMatrix;
}

//<mat4> scale(<float> sx, <float> sy, <float> sz);
function scale(sx,sy,sz){
  let resultMatrix = Matrix4();
  resultMatrix[indexTransform4(0,0)] = sx;
  resultMatrix[indexTransform4(1,1)] = sy;
  resultMatrix[indexTransform4(2,2)] = sz;
  return resultMatrix;
}

//<mat4> perspective(<float> d); // primerna vrednost je d=4
function perspective(d){
  let resultMatrix = Matrix4();
  resultMatrix[indexTransform4(3,2)] =  1/d;
  resultMatrix[indexTransform4(3,3)] = 0;
  return resultMatrix;
}

//Open file 
function openFile(event) {
  memset();
  let file = event.target.files[0];
  let reader = new FileReader();
  reader.onloadend = function() {
      fileData = reader.result;
      parseOBJ(fileData);      
  };  
  reader.readAsText(file);
}

function parseOBJ(fileData){
  var lines = fileData.split("\n");
  for(let iter = 0;iter<lines.length;iter++){
    let line = lines[iter].split(" ");
    if(line.length > 1){
      if(line[0] == "v"){
         parseVertex(line);
        }
      else if(line[0] == "f"){
          parseFace(line);
      }
      else if(line[0] == "l"){
         parseLight(line);
      }
      else if(line[0] == "m"){
         parseMaterial(line);
      }
    }
  }  
  calculateTransform();
}


function negateVector4(VectorA){
  let result = new Vector4();
  for(let iter = 0;iter < 4;iter++)
    result[iter] = - VectorA[iter];
  return result;
}


function negateVector3(VectorA){
  let result = new Vector3();
  for(let iter = 0;iter < 3;iter++)
    result[iter] = - VectorA[iter];
  return result;
}

function subtractVector3(vectorA,vectorB){
  if(vectorA.length==3 && vectorB.length==3){
     let result = new Vector3();
     for(let i=0;i<vectorA.length;i++){
       result[i] = vectorA[i] - vectorB[i];
     }
     return result;
  }
  console.log("ERROR DIMENSIONS");
}

function subtractVector4(vectorA,vectorB){
  if(vectorA.length==4 && vectorB.length==4){
     let result = new Vector4();
     for(let i=0;i<4;i++){
       result[i] = vectorA[i] - vectorB[i];
     }
     return result;
  }
  console.log("ERROR DIMENSIONS");
}

function multiplyScalar3(vector,scalar){
  if(vector.length!=3){
    alert("ERROR DIMENSION MULITPLY 3");
  }
  let result = new Vector3();
  for(let i=0;i<3;i++){
      result[i] = vector[i] * scalar;
  }
  return result;
}

function multiplyScalar4(vector,scalar){
  if(vector.length!=4){
    alert("ERROR DIMENSION MULITPLY 4");
  }
  let result = new Vector4();
  for(let i=0;i<4;i++){
      result[i] = vector[i] * scalar;
  }
  return result;
}

function addScalar3(vector,scalar){
  if(vector.length!=3){
    alert("ERROR DIMENSION MULITPLY 3");
  }
  let result = new Vector3();
  for(let i=0;i<3;i++){
      result[i] = vector[i] + scalar;
  }
  return result;
}

function addScalar4(vector,scalar){
  if(vector.length!=4){
    alert("ERROR DIMENSION MULITPLY 4");
  }
  let result = new Vector4();
  for(let i=0;i<4;i++){
      result[i] = vector[i] + scalar;
  }
  return result;
}

function sumVector4(a,b){
  if(a.length==4 && b.length==4){
    let result = new Vector4();
    for(let i=0;i<4;i++){
      result[i] = a[i] + b[i];
    }
    return result;
  }
  console.log("ERROR DIMENSIONS");
}

function sumVector3(a,b){
  if(a.length==3 && b.length==3){
    let result = new Vector3();
    for(let i=0;i<3;i++){
      result[i] = a[i] + b[i];
    }
    return result;
  }
  console.log("ERROR DIMENSIONS");
}

let cA = 0.3;
function produce3(vector4){
  let result = new Vector3();
  result[0]=vector4[0];
  result[1]=vector4[1];
  result[2]=vector4[2];
  return result;
}

function reflect(l,n){
  let lxn = 2.0 * dot(l,n);
  let projectN = multiplyScalar3(n,lxn);
  let subtractL = subtractVector3(projectN,l);
  return subtractL;
}

function vertexLight(idx){
  let color = Vector3();
  if(!isCalculated){
    return colorMap[idx];
  }
  if(controlAmbient == 1){
    color = materials.Ka;
  }
  let updateMatrix = Matrix4();
  updateMatrix = matrixMultiply(translate(0,0,-8),modelViewMatrix);
  updateMatrix = matrixMultiply(translate(width/2,height/2,0),updateMatrix);
  let vertexPosition = produce3(transform(updateMatrix,vereticies[idx]));
  for(let i=0;i<lightPositions.length;i++){
    let lightPosition = lightPositions[i];
    let N = normalize3(produce3(transform(updateMatrix,normals[i])));
    let L = normalize3(subtractVector3(lightPosition,vertexPosition));
    let E = normalize3(negateVector3(vertexPosition));
    let R = normalize3(reflect(L,N));
    let lxn = dot(L,N);
    //let 
    let lambert = lxn;
    let exr = dot(E,R);
    if(controlAmbient == 1){
      exr = Math.max(exr,0);
      lambert = Math.max(lambert,0);
    }
    let phong = Math.pow(exr,materials.Ns);
    let diffused = multiplyScalar3(materials.Kd,lambert);
    let specular = multiplyScalar3(materials.Ks,phong);
    let difusedSpecular = sumVector3(diffused,specular);
    let lightxDS = pointwise(lightColors[i],difusedSpecular);
    color = sumVector3(color,lightxDS);
  }  
  return color;
}


var el1 = document.getElementById("p1");
var el2 = document.getElementById("pn");

el1.addEventListener("click", modifyText);
el2.addEventListener("click", modifyText);



function pointwise(a,b){
  if(a.length == b.length){
    let result = Vector3();
    for(let iter=0;iter<a.length;iter++){
      result[iter] = a[iter] * b[iter];
    }  
    return result;
  }
}
let updateColors = [];

function updateLights(){
  for(let i=0;i<vertexCount;i++){
    console.log(i);
    updateColors.push(vertexLight(i));
    console.log(vertexLight(i));
  }
}
let controlDraw = 0;
function myFunction(){
  controlDraw++;
  controlDraw = controlDraw%3;
  console.log("CLICED");
}
let controlAmbient = 0
function myFunction2(){
  controlAmbient = 1 - controlAmbient;
}
let colorMap = [];
let isCalculated = true;
function myFunction3(){
  isCalculated = !isCalculated;
  colorMap = [];
  if(isCalculated){
    return ;
  }
  for(let iter = 0;iter<vereticies.length;iter++){
      let tmpColor = new Vector3();
      tmpColor[0] = Math.random();
      tmpColor[1] = Math.random();
      tmpColor[2] = Math.random();
      colorMap.push(tmpColor);
  }
}
/*
//Ligh calc
function parseOBJ(fileData){
  var lines = fileData.split("\n");
  for(let iter = 0;iter<lines.length;iter++){
    let line = lines[iter].split(" ");
    if(line.length > 1){
      if(line[0] == "v"){
         parseVertex(line);
        }
      if(line[0] == "f"){
          parseFace(line);
      }
    }
  }  
  calculateTransform();
}

function parseVertex(line){
  vertexCount++;
  let tmp_vertex = Vector4();
  for(let iter = 0;iter < 3;tmp_vertex[iter] = parseFloat(line[iter+1]),iter++);
  tmp_vertex[3] = 1;
  tmp_vertex=normalize(tmp_vertex);
  vereticies.push(tmp_vertex);
}

function parseFace(line){
  let tmp_face = Vector3();
  for(let iter = 0;iter < 3;tmp_face[iter] = parseFloat(line[iter+1]),iter++);
  faces.push(tmp_face);  
}

*/

function parseVertex(line){
  vertexCount++;
  let tmp_vertex = Vector4();
  for(let iter = 0;iter < 3;tmp_vertex[iter] = parseFloat(line[iter+1]),iter++);  
  tmp_vertex[3] = 1;
  tmp_vertex=normalize(tmp_vertex);
  vereticies.push(tmp_vertex);
  let tmp_normal = Vector4();
  for(let iter = 0;iter < 3;tmp_normal[iter] = parseFloat(line[iter+4],iter++));
  tmp_normal = normalize(tmp_normal);
  normals.push(tmp_normal);
}


//m Ka_r Ka_g Ka_b Kd_r Kd_g Kd_b Ks_r Ks_g Ks_b Ns
function parseMaterial(line){
  materials.Ka = Vector3();
  for(let iter = 1;iter <= 3;materials.Ka[iter-1]=parseFloat(line[iter]),iter++);
  materials.Kd = Vector3();
  for(let iter = 1;iter <= 3;materials.Kd[iter-1]=parseFloat(line[iter+3]),iter++);
  materials.Ks = Vector3();
  for(let iter = 1;iter <= 3;materials.Ks[iter-1]=parseFloat(line[iter+6]),iter++);
  materials.Ns = line[line.length-1];
}

function parseLight(line){
  lightCount++;
  let tmp_light = Vector3();
  for(let iter = 0;iter < 3;tmp_light[iter] = parseFloat(line[iter+1]),iter++);
  lightPositions.push(tmp_light);
  let tmp_color = Vector3();
  for(let iter = 0;iter < 3;tmp_color[iter] = parseFloat(line[iter+4],iter++));
  console.log(tmp_color);
  lightColors.push(tmp_color);
}

function parseFace(line){
  let tmp_face = Vector3();
  for(let iter = 0;iter < 3;tmp_face[iter] = parseFloat(line[iter+1]),iter++);
  faces.push(tmp_face);  
}


document.onkeydown =  function (e){
  switch(e.which) {
      case 87: // W
          modelViewMatrix = matrixMultiply(rotateX(delta),modelViewMatrix);
          break;
      case 83: // S
          modelViewMatrix = matrixMultiply(rotateX(-delta),modelViewMatrix);
          break;
      case 68: // D
          modelViewMatrix = matrixMultiply(rotateY(-delta),modelViewMatrix);
          break;
      case 65: // A
          modelViewMatrix = matrixMultiply(rotateY(delta),modelViewMatrix);
          break;
      case 69: // E
          modelViewMatrix = matrixMultiply(rotateZ(-delta),modelViewMatrix);
          break;
      case 81: // Q
          modelViewMatrix = matrixMultiply(rotateZ(delta),modelViewMatrix);
          break;
      case 38: // UP
          modelViewMatrix = matrixMultiply(translate(0, -delta, 0),modelViewMatrix);
          break;
      case 40: // DOWN
          modelViewMatrix = matrixMultiply(translate(0, delta, 0),modelViewMatrix);
          break;
      case 39: // RIGHT
          modelViewMatrix = matrixMultiply(translate(delta, 0, 0),modelViewMatrix);
          break;
      case 37: // LEFT
          modelViewMatrix = matrixMultiply(translate(-delta, 0, 0),modelViewMatrix);
          break;  
      case 107: // +
          modelViewMatrix = matrixMultiply(scale(1.1, 1.1, 1.1), modelViewMatrix);
          break;
      case 109: // -
          modelViewMatrix = matrixMultiply(scale(1/1.1, 1/1.1, 1/1.1), modelViewMatrix);
          break;
      case 82: // R
          modelViewMatrix = Matrix4();
          modelViewMatrix = matrixMultiply(scale(100, -100, 100), modelViewMatrix); 
          break;    
      default:
          return;
  }
  if(vertexCount>0)
    calculateTransform();
}